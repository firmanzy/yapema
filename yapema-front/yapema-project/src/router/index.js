import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/Project',
    name: 'Project',
    component: () => import(/* webpackChunkName: "about" */ '../views/Mosques.vue')
  },
  {
    path: '/location',
    name: 'Location',
    component: () => import(/* webpackChunkName: "about" */ '../views/Location.vue')
  },
  {
    path: '/mosque/id',
    name: 'Mosque',
    component: () => import(/* webpackChunkName: "about" */ '../views/Mosque.vue')
  },
  {
    path: '/locationindex',
    name: 'location_tampil',
    component: () => import(/* webpackChunkName: "about" */'../views/location_tampil.vue')
  },
  {
    path: '/locationedit/:id',
    name: 'location_edit',
    component: () => import(/* webpackChunkName: "about" */'../views/location_edit.vue')
  },
  {
    path: '/locationdelete',
    name: 'location_create',
    component: () => import(/* webpackChunkName: "about" */'../views/location_create.vue')
  }

  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

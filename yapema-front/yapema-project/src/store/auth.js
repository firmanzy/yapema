import axios from 'axios';

export default{
    namespaced: true,
    state: {
        token: '',
        user: {},
    },
    mutations: {
        setToken: (state, payload) => {
            state.token = payload
        },
        setUser: (state, payload) => {
            state.user = payload
        },
    },
    actions: {
        setToken: ({commit,dispatch}, payload) => {
            commit('setToken', payload)
            dispatch('checkToken', payload)
        },
        checkToken: ({commit}, payload) => {
            let config = {
                method: "post",
                url: 'localhost:8000/api/auth/check',
                headers: {
                    'Authorization': 'Bearer' + payload,
                },
            }
        // setUser: ({commit}, payload) => {
        //     commit('setUser', payload)
        // },
        
        axios(config)
            .then((response) => {
                commit('setUser', response.data)
            })
            .catch(() => {
                commit('setUser', {})
                commit('setToken', '')
            })
        },
    },
    getters: {
        user: state => state.user,
        token: state => state.token,
        guest: state => Object.keys(state.user).length === 0,
    }
}

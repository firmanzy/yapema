<?php

namespace App\Http\Controllers\Auth;
use App\Donatur;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /**
     * login
     *
     * @param  mixed $request
     * @return void
     */
    public function __invoke(Request $request)
    {
        $allrequest = $request->all();
        $validator = Validator::make($allrequest,[
            'email' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json([
                'success' => false,
                'message' => 'Email atau Password tidak ditemukan'
            ], 401);
        }

        return response()->json([
            'success' => true,
            'message' => 'Donatur berhasil login',
            'data' =>[
                'donatur' => auth()->donatur(),
                'token'=> $token
            ]
        ], 200);
    }
    
}
<?php

namespace App\Http\Controllers\Auth;

use App\Donatur;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\UserRegisteredEvent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name' => 'required',
            'email' => 'required|unique:donaturs,email|email',
            'password' => 'required'
        ]);

        

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $donatur = Donatur::create($allRequest);

        $donatur->update([
            'password' => Hash::make($request->password)
        ]);

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(30),
            'donatur_id' => $donatur->id
        ]);

        // Kirim email OTP Code terlebih dahulu seharusnya
        // event(new UserRegisteredEvent($otp_code));

        return response()->json([
            'success' => true,
            'message' => 'New user has been registered',
            'data' => [
                'donatur' => $donatur,
                'otp_code' => $otp_code
            ]
        ]);
    }
}

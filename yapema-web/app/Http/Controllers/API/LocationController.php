<?php

namespace App\Http\Controllers\API;

use App\Location;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table locations
        $locations = Location::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data location',
            'data'    => $location  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find location by ID
        $location = Location::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data location',
            'data'    => $location 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'address'   => 'required',
            'family'   => 'required',
            'land_area' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = auth()->user();

        //save to database
        $location = Location::create([
            'name'     => $request->name_location,
            'address' => $request->address,
            'family' => $request->family,
            'land_area' => $request->land_area,
            
        ]);

        //success save to database
        if($location) {

            return response()->json([
                'success' => true,
                'message' => 'location Created',
                'data'    => $location  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'location Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $location
     * @return void
     */
    public function update(Request $request, Location $location)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'address'   => 'required',
            'family'   => 'required',
            'land_area' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }


   
        //find location by ID
        $location = Location::findOrFail($location->id);

        if($location) {
            $user = auth()->user();
            if($location->user_id != $user->id){

                return response()->json([
                    'success' => false,
                    'message' => 'Forbidden to Update location',
                    'data'    => $location  
                ], 200);
    

                
            }

             //update location
             $location->update([
                'name'     => $request->name_location,
            'address' => $request->address,
            'family' => $request->family,
            'land_area' => $request->land_area,n
            ]);

            return response()->json([
                'success' => true,
                'message' => 'location Updated',
                'data'    => $location  
            ], 200);

           

        }

        //data location not found
        return response()->json([
            'success' => false,
            'message' => 'location Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find location by ID
        $location = Location::findOrfail($id);

        if($location) {

            //delete location
            $location->delete();

            return response()->json([
                'success' => true,
                'message' => 'location Deleted',
            ], 200);

        }

        //data location not found
        return response()->json([
            'success' => false,
            'message' => 'location Not Found',
        ], 404);
    }
}

<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Mosque extends Model
{
    protected $fillable = ['name_mosque','address', 'donatur_id', 'photo_mosque', 'progress_mosque'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();
        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
    public function user()
    {
        return $this->belongsTo('\App\Donatur');
    }
}
